# mneme

Emacs package to handle conflicting recentf-lists between emacs instances. 

## Installation
**straight.el**
```elisp
(use-package mneme
  :straight (mneme :type git
		   :host nil
		   :repo "https://codeberg.org/k0kytos/mneme.git"))
```

**otherwise**

You can install this package by cloning the project or downloading `mneme.el`, adding it to your load-path and calling
```elisp
(require 'mneme)
```
within your configuration.


## Customization
This package is not meant to be highly flexible as I mainly prefer such processes to remain invisible to the user. Still there are two variables to control the behaviour of mneme.

**mneme--verbosity**

Sets the verbosity level of mneme.
Mneme does substantially more than just one call per execution. Therefore the \*message\* buffer can get quite cluttered, especially when you set `mneme--interval' to a non-zero number.  This variable gives you control over the amount of messages being sent.  It will not affect anything outside of this package.

`0`:  Hide all messages

`1`:  Print confirmation of a successful execution

`2`:  Print everything

Defaults to 1.

**mneme--interval**

Sync the `recentf-list` once per interval. Setting this to a nonzero value will setup a periodic execution for the user which runs every `mneme--interval` seconds to synchronize and save the `recentf-save-list`. Setting this variable to `0` disables this functionality. If nonzero it is encouraged to set `recentf-auto-cleanup` to `'never` as `recentf-save-list` will now trigger a cleanup as well.

Defaults to 0.