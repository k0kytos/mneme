;;; mneme.el --- Synchronize the recent files list between Emacs instances



;; Copyright (C) 2022 K0kytos

;; Author: K0kytos <k0kytos@pm.me>
;; Created: 22.03.2022
;; Version: 1.0
;; Keywords: recentf
;; X-URL: https://codeberg.org/k0kytos/mneme

;; This file is NOT part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This package helps synchronizing the recent files list between Emacs
;; instances.  Without it, each Emacs instance manages its own recent files
;; list.  The last one to close persistently saves its list into
;; `save-file'; all files recently opened by other instances are
;; overwritten.
;;
;; This implementation utilizes helper variables to keep track of changes
;; which get merged with `recentf-save-file' when `recentf-save-list' is
;; called.  To prevent collisions on write a manual locking mechanism is
;; used to ensure no invalid recentf state is used to compute the next
;; one.

;;; Code:

(require 'recentf)

(defgroup mneme nil
  "Conflict management for recentf."
  :group 'mneme
  :prefix "mneme--")


(defcustom mneme--interval 0
  "Sync the `recentf-list' once per interval.
Set to `0' if no periodic synchronization should be set up by mneme.

Defaults to `0'."
  :group 'mneme
  :type 'integer)


(defcustom mneme--verbosity 1
  "Set the verbosity level of `mneme'.

`mneme' does substantially more than just one call per execution.
Therefore the *message* buffer can get quite cluttered, especially when you
set `mneme--interval' to a non-zero number.  This variable gives you
control over the amount of messages being sent.  It will not affect anything
outside of this package.

`0':  Hide all messages
`1':  Print confirmation of a successful execution
`2':  Print everything

Defaults to 1."
  :group 'mneme
  :type 'integer)


(defvar mneme--new-files nil
  "Contains all newly opened files within this instance.")


(defvar mneme--deleted-files nil
  "Contains all dialog-deleted files within this instance.")


(defvar mneme--lockfile
  (concat recentf-save-file ".lock")
  "File used as a lockfile for recentf list merges.")


(defun mneme--lock-or-wait (&optional iter)
  "Create the lockfile or recursively try again after waiting.
ITER signals the state of the binary exponential backoff."
  (condition-case error
      (with-temp-buffer (write-file mneme--lockfile))
    (error
     (while (not (eq 0 (random (ash 2 iter))))
       (sleep-for 0 100))
     (mneme--lock-or-wait (+ 1 iter)))))


(defadvice recentf-track-opened-file (after add-recent-file activate compile)
  "Pushes a filepath PATH to the `mneme--new-files' variable."
  (if
      (bound-and-true-p mneme-mode)
      (if
          (buffer-file-name)
          (let
              ((file
                (expand-file-name buffer-file-name)))
            (when
                (recentf-include-p file)
              (setq mneme--new-files
                    (cons file mneme--new-files)))))))


(defadvice recentf-edit-list-validate (before remove-deleted-recent-file activate compile)
  "Pushes a to the `mneme--deleted-files' variable and IGNORE the rest."
  (if
      (bound-and-true-p mneme-mode)
      (dolist
          (e recentf-edit-list)
        (setq mneme--deleted-files
              (cons e mneme--deleted-files)))))


(defadvice ask-user-about-lock (before recentf-lock-handler activate compile)
  "Directly handle manual locks of `mneme--lockfile' by throwing an error.
This will skip the execution of `ask-user-about-lock' and allow an automatic
backoff and retries."
  (if
      (bound-and-true-p mneme-mode)
      (if
          (string-equal
           (expand-file-name file)
           mneme--lockfile)
          (signal 'file-locked
                  (list file opponent)))))


(defadvice recentf-load-list (after recentf-load-list-helper activate compile)
  "Apply cached differences to `recentf-list' after `recentf-load-list' is called."
  (if
      (bound-and-true-p mneme-mode)
      (setq recentf-list
            (append mneme--new-files recentf-list))
    (dolist
        (e mneme--deleted-files)
      (setq recentf-list
            (delete e recentf-list)))))


(defadvice recentf-save-list (before sync-recentf-entries activate compile)
  "Synchronize the local list with that of `recentf-file'.
Saving to `recentf-save-file' is dangerous due to locks happening only when
writing while calculations could occur simultaneously leading to overrides.
Handle this via a manual lock construct."
  (if
      (bound-and-true-p mneme-mode)
      (let*
          ((cond
            (< mneme--verbosity 2))
           (inhibit-message cond)
           (message-log-max
            (if cond nil message-log-max)))
        (mneme--lock-or-wait)
        (recentf-load-list)
        (setq mneme--new-files nil)
        (setq mneme--deleted-files nil)
        (recentf-cleanup))))


(defadvice recentf-save-list (after unblock-recentf-file activate compile)
  "Unlock the `recentf-file' when done syncing."
  (if
      (bound-and-true-p mneme-mode)
      (let*
          ((cond
            (< mneme--verbosity 2))
           (inhibit-message cond)
           (message-log-max
            (if cond nil message-log-max)))
        (delete-file mneme--lockfile))))


(make-variable-buffer-local
 (defvar timer-object nil
   "Timer for running the synchronisation."))


;;;###autoload
(define-minor-mode mneme-mode
  "Global minor mode wrapping around `recentf-mode'.
Mneme provides synchronisation of the `recentf-list' when using multiple
instances."
  :global t
  :group 'mneme
  :lighter " mneme"
  (if mneme-mode
      (unless (zerop mneme--interval)
        (setq timer-object
              (run-with-timer
               mneme--interval
               mneme--interval
               #'(lambda ()
                   (let* ((cond (zerop mneme--verbosity))
                          (inhibit-message cond)
                          (message-log-max (if cond nil message-log-max)))
                     (recentf-save-list))))))
    (when (symbol-value timer-object)
      (cancel-timer timer-object))))


(provide 'mneme)
;;; mneme.el ends here
